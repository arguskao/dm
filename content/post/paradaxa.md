+++
author = "高藥師"
categories = ["藥物新知"]
tags = ["普拴達"]
date = "2018-12-27"
title = "普栓達與糖尿病的關係"

+++

[普栓達](https://www.liverx.net/2015/12/23/%E6%99%AE%E6%A0%93%E9%81%94pradaxa%E6%96%B0%E4%B8%80%E4%BB%A3%E7%9A%84%E6%8A%97%E5%87%9D%E8%A1%80%E5%8A%91/)是一個抗凝血劑，乍看之下好像和[糖尿病](https://www.liverx.net/tag/%E7%B3%96%E5%B0%BF%E7%97%85/)沒有一毛錢關係。但是年齡、性別、氣候與家族遺傳是中風者無法改變的危險因子，<font color=red>高血壓、糖尿病、肥胖</font>、情緒緊張、壓力、熬夜、抽菸、喝酒卻是可以藉由許多方法改善的。高血壓是出血性中風的發生主因，有效控制血壓，可同時預防腦中風、冠狀動脈心臟病等其他相關疾病。

自2014.6.1健保局共核准給付三種 新型口服抗凝血劑: Dabigatran(Pradaxa), Rivaroxaban(Xarelto), Apixaban(Eliquis)
對於非瓣膜性心房顫動, 肌酸酐清除率(Ccr) > 30 ml/min, 未有嚴重心臟瓣膜疾病且未接受過機械或生物瓣膜置換手術者，提供Warfarin(Coumadin)之外更便利使用的選擇。

延伸閱讀：[心臟小站](http://www.ksmale.net/news/2014NOAC)


+++
author = "高藥師"
categories = ["糖尿病保健食品"]
tags = ["保健食品","單方","鉻"]
date = "2019-06-02"
title = "加特福GT&F奶粉"

+++

[GT&F奶粉](http://www.cgprdi.org.tw/functionalfood/helth_inf/hel_viewtopic.asp?id=236)是加特福生物科技股份有限公司所生產的，如果沒記錯，應該是最早申請的，應該也賺了不少。成分是[鉻](https://dm.hi29.net/tags/%E9%89%BB/)。

核可功效：「對<font color=red>禁食血糖值偏高者</font>，具有輔助調節作用。」

警語：「1.本產品並非治療及矯正疾病的藥品；2.糖尿病病患仍須就醫治療；3.每日乙包，多食無益」。

歸類在[單方](https://dm.hi29.net/tags/%E5%96%AE%E6%96%B9/)類別。

pchome上價錢一盒30包1350元，價格中等。


下一篇[膳纖熟飯](https://dm.hi29.net/post/rice/)
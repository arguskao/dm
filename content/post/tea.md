+++
author = "高藥師"
categories = ["糖尿病保健食品"]
tags =["保健食品","單方","糊精"]
date = "2019-04-18"
title = "愛之味健康糖切茶"
+++

[愛之味健康糖切茶](http://www.cgprdi.org.tw/functionalfood/helth_inf/hel_viewtopic.asp?id=124)是愛之味股份有限公司的產品，成分是難消化性[糊精](https://dm.hi29.net/tags/%E7%B3%8A%E7%B2%BE/)。

核可的保健功效是經動物實驗證實，本產品對於<font color=red>禁食血糖偏高者</font>，具有輔助調節作用。

這個產品歸在[單方](https://dm.hi29.net/tags/%E5%96%AE%E6%96%B9/)類別。

根據pchome上面賣價，24入830元，一個月大約1000元，還算不貴。

下一篇[三多好纖](https://dm.hi29.net/post/fiber/)
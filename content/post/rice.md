+++
author = "高藥師"
categories = ["糖尿病保健食品"]
tags = ["保健食品","糊精"]
date = "2019-04-25"
title = "膳纖熟飯"


+++

[膳纖熟飯](http://www.cgprdi.org.tw/functionalfood/helth_inf/hel_viewtopic.asp?id=100)是南僑化學工業股份有限公司生產的，成分是難消化性[糊精](https://dm.hi29.net/tags/%E7%B3%8A%E7%B2%BE/),穀類。

核可功效是：經動物實驗證實，<font color=red>有助於降低空腹血糖值</font>。

Pchome賣價12盒780元，一個月等於1950元，<font color=red>有點貴</font>。

下一篇[愛之味健康糖切茶](https://dm.hi29.net/post/tea/)